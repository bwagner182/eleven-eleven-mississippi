jQuery(document).ready(
    function($){

            $(".required [name='cool_timeline_license_registration[cool_timeline-purchase-code]']").attr('required','required');
            $(".required [name='cool_timeline_license_registration[cool_timeline-purchase-code]']").attr('oninvalid','this.setCustomValidity("Purchase code can not be empty!")');

            $(".required [name='cool_timeline_license_registration[cool_timeline-client-emailid]']").attr('type','email');

            if( $(".cool_timeline_verification_enable").length > 0 ){
                $("#cool_timeline-activation-button #submit").attr('disabled','disabled');
                $('#cool_timeline-activation-button').addClass('hidden');
                $('#cool_timeline-verify-permission').addClass('hidden');
                $('.cool_timeline-notice-red:not(".uninstall")').addClass('hidden');
            }else{
                $("#cool_timeline-uninstall-license").attr('disabled','disabled');
                $('#cool_timeline-deactivation-button').addClass('hidden');
            }
    
            // product uninstall hook
            $license_code = $("#coolpluginslicense_registration[cool_timeline-purchase-code]").value;
            $("a#cool_timeline-uninstall-license").on('click',function(e){
                e.preventDefault();
                if( $("a#cool_timeline-uninstall-license").attr('disabled') ){
                    return;
                }
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: ajax_object.ajax_url,
                  data: {
                        action:'cool_timeline_uninstall_license'
                        },
                beforeSend: function(data){
                    $("a#cool_timeline-uninstall-license").html("Uninstalling license... <span class='cool_timeline-loading'></span>");
                },
                  success: function (response) {
                        $("a#cool_timeline-uninstall-license").html("License Uninstalled!");
                        alert(response);
                        location.reload();
                  }
              })
    
            });
    
            var request = $('#ccpa_plugin_support').value;
            $("#cool_timeline_support_btn").on('click',function(){
            $.ajax({
              type: "POST",
              dataType: "json",
              url: ajax_object.ajax_url,
              data: {action:'cool_timeline_submit_ticket',
                    request:request
                    },
              success: function (response) {                  
                          console.log(response)
              }
          })
          });
    });