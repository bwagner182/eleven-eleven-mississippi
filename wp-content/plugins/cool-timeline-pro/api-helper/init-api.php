<?php
class CTL_ApiConf{
    const PLUGIN_NAME = 'Cool Timeline Pro';
    const PLUGIN_VERSION = '2.8.7';
    const PLUGIN_PREFIX = 'cool_timeline';
    const PLUGIN_AUTH_PAGE = 'ctl-settings';
	const PLUGIN_URL = CTP_PLUGIN_URL;
}

	require_once 'api-helper-functions.php';
    require_once 'class.settings-api.php';
    require_once 'api-auth-settings.php';
    	
	new CTL_Settings();